# 如何在 Ubuntu 上使用新的 DEB822 apt格式

> 来源于: 
> https://www.techrepublic.com/article/how-to-use-the-new-deb822-apt-format-on-ubuntu/

## 概要

作者: Jack Wallen

本文背景: 解释了在大多数新的基于 `debian` 的Linux发行版中发现的新的`DEB822` apt源代码格式。

### 尝试了解什么是 DEB822 的 apt 源描述

1. 如果你是一个 `Ubuntu` 的长期用户，这个平台的最新版本带来了一些新东西，可能会让你困惑一段时间。
来说点新鲜的东西: `822 apt源码格式`。在这之前你可能习惯于`apt`源文件只包含一行代码，如

    ```
    deb http://repository.spotify.com stable non-free
    ```

2. 从 `Ubuntu 20.10` 开始，该格式已经从单行格式变为多行格式，称为 `DEB822`。
`DEB822`条目可以包含以下规范，例如:

    ```conf
    # 定义源代码的格式化名称(类似于 Systemd Service Unit 单元的命名一样)
    X-Repolib-Name: Defines the formatted name for the source
    # 启用或禁用存储库(可选: yes(默认)|no 不强制要求)
    Enabled: To enable or disable the repository
    # 定义它是二进制还是源代码存储库(必选: deb [deb-src] 强制要求)
    Types: Defines if it’s a binary or source code repository
    # 给出存储库的地址(必选: http://)
    URIs: Gives the address for the repository
    # 与所提供uri相关的精确路径，或者以分发版本的形式(必选: ...)
    Suites: An exact path in relation to the provided URIs or in the form of a distribution version
    # 指定套件中单个发行版本的不同部分(必选: ...)
    Components: Specify different sections of a single distribution version present in a Suite
    # 存储库的默认镜像地址(http)
    X-Repolib-Default-Mirror: The address of the default mirror for the repository
    ```

3. 所以一个典型的apt源文件应该是这样的:
    ```
    X-Repolib-Name: Pop_OS System Sources
    Enabled: yes
    Types: deb deb-src
    URIs: http://us.archive.ubuntu.com/ubuntu/
    Suites: groovy groovy-security groovy-updates groovy-backports
    Components: main restricted universe multiverse
    X-Repolib-Default-Mirror: http://us.archive.ubuntu.com/ubuntu/
    ```


### 结论

事实上，旧的 `/etc/apt/sources.list` 文件(在某些发行版中)已弃用。

取而代之的是 `/etc/apt/sources.list.d/system.sources`。

了解这种新的格式，因为从现在开始您将会使用它。

