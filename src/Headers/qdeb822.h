#ifndef QDEB822_H
#define QDEB822_H

#include <QVariant>

class QDeb822
{
public:
    QVariant toVariant() const;
    QString toString() const;

    static QDeb822 fromVariant(const QVariant &variant);
    static QDeb822 fromString(const QString &data);

    QVariantMap toVariantMap(const QString &key);

protected:
    void parse();
    void print();

private:
    QDeb822();

private:
    QString m_data;
    QVariantList m_variants;
};

#endif // QDEB822_H
