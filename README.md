# QDeb822

> 用于解析和生成deb822格式数据的Qt库
> 
> 了解 [DEB822 源格式的详细的内容与字段解释](https://repolib.readthedocs.io/en/latest/deb822-format.html)\
> 了解 [如何在 Ubuntu 上使用新的 DEB822 apt格式(网摘)](about-deb822.md)



#### 软件包结构

1. 构建-非实际安装(指定目录)-查看结构
    
    ```bash
    mkdir build && cd build
    cmake ..
    make DESTDIR=./extract install
    tree extract/
    extract/
    └── usr
        └── local
            ├── include
            │   └── QDeb822
            │       ├── QDeb822
            │       └── qdeb822.h
            └── lib
                ├── cmake
                │   └── QDeb822         # 该库提供的 CMake 模块
                │       └── QDeb822Config.cmake
                ├── libqdeb822.so -> libqdeb822.so.0.1
                ├── libqdeb822.so.0.1
                ├── pkgconfig
                │   └── qdeb822.pc      # 该库提供的 pkg-config (第三方库)的描述信息
                └── qt5
                    └── mkspecs
                        └── modules
                            └── qt_lib_qdeb822.pri
                                        # 该库提供的 Qt 模块的描述信息
    ```

#### 安装教程

1. 获取仓库代码并进行构建打包

    ```
    git clone https://gitee.com/mbsr/qdeb822
    cd qdeb822
    dpkg-buildpackage -us -uc -B
    sudo apt install ../libqdeb822_0.1_amd64.deb ../libqdeb822-dev_0.1_amd64.deb
    ```

#### 使用说明
1. 在 CMake 中使用

    ```cmake
    find_package(QDeb822)
    target_link_libraries(${PROJECT_NAME} ${QDeb822_LIBRARY})
    ```

2. 在 QMake 中使用

    ```qmake
    QT += qdeb822
    ```

3. 在命令行或 Makefile 构建中使用(PkgConfig)

    ```bash
    # 或使用 pkg-config qdeb822 --cflags --libs (大概)
    pkg-config qdeb822
    ```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

