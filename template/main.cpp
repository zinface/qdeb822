#include "qdeb822.h"

#include <QFile>
#include <QDebug>

int main(int argc, char *argv[])
{
    QFile status("/var/lib/dpkg/status");
    status.open(QIODevice::ReadOnly | QIODevice::Text);
    QDeb822 deb822 = QDeb822::fromString(status.readAll());
    qDebug().noquote() << deb822.toVariantMap("Package")["dde-desktop"];
}
