cmake_minimum_required(VERSION 3.14)

project(template LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include_directories(../src/Headers)

find_package(Qt5 COMPONENTS Core LinguistTools REQUIRED)

add_executable(template
  main.cpp
  ${TS_FILES}
)
target_link_libraries(template Qt5::Core qdeb822)
